<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii_blog',
    'username' => '',
    'password' => '',
    'charset' => 'utf8',
    'tablePrefix' => 'tbl_'
];
