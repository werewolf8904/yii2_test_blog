<?php

return [
    'translatedLanguages'=>[
        'uk'=>'Український',
        'ru'=>'Руский',
        'en'=>'English'

    ],
    'defaultLanguage'=>'uk',
    'image_root'=>'/images/',
    'thumbs_root'=>'/images/thumbs/'
];
