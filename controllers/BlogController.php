<?php
/**
 * Created by PhpStorm.
 * User: Yurec
 * Date: 30.01.2017
 * Time: 9:54
 */

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Post;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;

class BlogController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
        ],
        ];
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays blog.
     *
     * @return string
     */
    public function actionIndex()
    {

        $post = new Post();
        $pagination= new Pagination(['defaultPageSize' => 10,
            'totalCount' =>Post::find()
                ->where(['publish_status'=>'publish'])
                ->count(),
        ]);

            return $this->render('index',['posts'=>$post->getPublishedPosts($pagination),'pagination' => $pagination]);



    }
    public function actionView($slug)
    {

        return $this->render('view',['model'=>$this->findModel($slug)]);

    }
    protected function findModel($slug)
    {

        if (($model = Post::findOne(['slug'=>$slug,'publish_status'=>'publish'])) !== null) {

            return $model;

        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}