<?php
/**
 * Created by PhpStorm.
 * User: Yurik
 * Date: 29.01.2017
 * Time: 23:57
 */


return [
    'Test blog' => 'Тестовый блог',
    'Home' => 'На главную',
    'About' => 'О сайте',
    'Blog' => 'Блог',
    'Login' => 'Вход',
    'Logout' => 'Выход',
    'Administration' => 'Администрирование',
    'Publish date' => 'Дата публикации',
    'Update date' => 'Дата обновления',
    'Posts' => 'Посты',
    'Create' => 'Создать',
    'Draft' => 'Не опублтковано',
    'Publish' => 'Опубликовано',
    'Select status' => 'Виберите статус',
    'Update' => 'Обновить',
    'Delete' => 'Удалить',
    'ID' => 'ІД',
    'Title' => 'Заголовок',
    'Content' =>'Контент',
    'Image' => 'Изображение',
    'Publish status' => 'Статус публикации',
    'Slug' => 'Слаг',
    'Publish date: {0,date,full}-{0,time}' => 'Дата публикации: {0,date,full}-{0,time}',
    'Update date: {0,date,full}-{0,time}' => 'Дата обновления:  {0,date,full}-{0,time}',
    'Update {modelClass}: '=>'Обновить {modelClass}: ',
    'Post'=>'Пост',
    'Create Post'=>'Создать пост'

];