<?php
/**
 * Created by PhpStorm.
 * User: Yurik
 * Date: 29.01.2017
 * Time: 23:57
 */
return [
    'Test blog' => 'Тестовий блог',
    'Home' => 'На головну',
    'About' => 'Про сайт',
    'Blog' => 'Блог',
    'Login' => 'Вхід',
    'Logout' => 'Вихід',
    'Administration' => 'Адміністрування',
    'Publish date' => 'Дата публікації',
    'Update date' => 'Дата оновлення',
    'Posts' => 'Пости',
    'Create' => 'Створити',
    'Draft' => 'Не опубліковано',
    'Publish' => 'Опубліковано',
    'Select status' => 'Виберіть статус',
    'Update' => 'Оновити',
    'Delete' => 'Видалити',
    'ID' => 'ІД',
    'Title' => 'Заголовок',
    'Content' =>'Контент',
    'Image' => 'Зображення',
    'Publish status' => 'Статус публікації',
    'Slug' => 'Слаг',
    'Publish date: {0,date,full}-{0,time}' => 'Дата публікації: {0,date,full}-{0,time}',
    'Update date: {0,date,full}-{0,time}' => 'Дата оновлення:  {0,date,full}-{0,time}',
    'Update {modelClass}: '=>'Оновити {modelClass}: ',
    'Post'=>'Пост',
    'Create Post'=>'Створити пост'

];