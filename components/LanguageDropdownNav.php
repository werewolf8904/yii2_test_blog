<?php
/**
 * Created by PhpStorm.
 * User: Yurec
 * Date: 31.01.2017
 * Time: 8:59
 */

namespace app\components;
use yii;
use yii\bootstrap\nav;
use yii\bootstrap\Dropdown;
use yii\bootstrap\ButtonDropdown;
class LanguageDropdownNav extends  Nav
{
    public $_labels;
    private $current;
    private $_isError;

    public function init()
    {
        $route = Yii::$app->controller->route;
        $appLanguage = Yii::$app->language;
        $params = $_GET;
        $this->_isError = $route === Yii::$app->errorHandler->errorAction;

        array_unshift($params, '/' . $route);

        foreach (Yii::$app->urlManager->languages as $language) {
            $isWildcard = substr($language, -2) === '-*';
            if (
                $language === $appLanguage ||
                // Also check for wildcard language
                $isWildcard && substr($appLanguage, 0, 2) === substr($language, 0, 2)
            ) {
                if ($isWildcard) {
                    $language = substr($language, 0, 2);
                }
                $this->current=$this->label($language);
                continue;   // Exclude the current language
            }
            if ($isWildcard) {
                $language = substr($language, 0, 2);
            }
            $params['language'] = $language;
            $this->items[] = [
                'label' => $this->label($language),
                'url' => $params,
            ];
        }
        parent::init();
    }

    public function run()
    {
        // Only show this widget if we're not on the error page
        if ($this->_isError) {
            return '';
        } else {
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    [
                        'label' => $this->current,
                        'items' => $this->items,
                    ],
                ],
            ]);
        }
    }

    public function label($code)
    {
        if ($this->_labels === null) {
            $this->_labels = [
                'de' => Yii::t('app', 'German'),
                'fr' => Yii::t('app', 'Russian'),
                'en' => Yii::t('app', 'English'),
            ];
        }

        return isset($this->_labels[$code]) ? $this->_labels[$code] : null;
    }
}