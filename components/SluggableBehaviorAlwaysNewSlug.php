<?php
/**
 * Created by PhpStorm.
 * User: Yurec
 * Date: 31.01.2017
 * Time: 12:49
 */

namespace app\components;
use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * Для оновлення slug при кожному update
 * Class SluggableBehaviorAlwaysNewSlug
 * @package app\components
 */
class SluggableBehaviorAlwaysNewSlug extends SluggableBehavior
{
    protected function isNewSlugNeeded()
    {
        return true;
    }
}