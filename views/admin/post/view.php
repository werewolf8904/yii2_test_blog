<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
  <?php
          $wid_set=[
              'model' => $model,
              'attributes' => [
                  'id',
              ],
          ];
        
          foreach (Yii::$app->params['translatedLanguages'] as $l => $lang) :
                if($l === Yii::$app->params['defaultLanguage'])
                    $suffix = '';
                else
                    $suffix = '_' . $l;
              
              array_push($wid_set['attributes'],'title'.$suffix);
              array_push($wid_set['attributes'],'content'.$suffix.':ntext');
        
          endforeach;
  
          $wid_set['attributes'][]=[
              'attribute'=>'image',
              'value'=>Yii::$app->params['image_root'].$model->image_route,
              'format' => ['image',['width'=>'350','height'=>'350']],
          ];
          $wid_set['attributes'][]='publish_status';
          $wid_set['attributes'][]='slug';
          $wid_set['attributes'][]='publish_date';
          $wid_set['attributes'][]='update_date';
          ?>

     <?=DetailView::widget($wid_set)?>



</div>
