<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \yii\helpers\StringHelper;
use \yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Posts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Post'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            [
        'attribute'=>'content',
                'value'=>function($model){
                   return StringHelper::truncate($model->content,300,'...');}
            ],

            'image_route',
            [
                'label' => Yii::t('app', 'Image'),
                'format' => 'raw',
                'value' => function($data){
                    return Html::img(Yii::$app->params['thumbs_root'].$data->image_route,[
                        'alt'=>'image',
                        'style' => 'width:100px;'
                    ]);
                },
            ],
            'publish_status',
            'slug',
            'publish_date',
            'update_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
