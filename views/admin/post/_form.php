<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php foreach (Yii::$app->params['translatedLanguages'] as $l => $lang) :
   if($l === Yii::$app->params['defaultLanguage'])
        $suffix = '';
    else
        $suffix = '_' . $l;
    ?>
    <?= $form->field($model, 'title'. $suffix)->textInput(['maxlength' => true,'minlength' => true]) ?>

    <?= $form->field($model, 'content'. $suffix)->textarea(['rows' => 6]) ?>
    <?php endforeach; ?>

    <?= $model->isNewRecord?'':Html::img(Yii::$app->params['image_root'].$model->image_route,['width'=>'350','height'=>'350']) ?>

    <?= $form->field($model, 'image')->fileInput() ?>

    <?= $form->field($model, 'publish_status')->dropDownList([ 'draft' => Yii::t('app', 'Draft'), 'publish' => Yii::t('app', 'Publish'), ], ['prompt' => Yii::t('app', 'Select status') ]) ?>

    <?= $form->field($model, 'slug')->textInput(['disabled' => true]) ?>

    <?=$form->field($model, 'publish_date')->textInput(['disabled' => true]) ?>

    <?= $form->field($model, 'update_date')->textInput(['disabled' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
