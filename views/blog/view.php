<?php
/**
 * Created by PhpStorm.
 * User: Yurik
 * Date: 29.01.2017
 * Time: 12:05
 */
use common\models\Comment;
use common\models\TagPost;
use yii\helpers\Html;
/* @var $model common\models\Post */
/* @var \frontend\models\CommentForm $commentForm \;
/* @var TagPost $post */
$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= $model->title ?></h1>


<div>
    <?= Html::img(Yii::$app->params['image_root'].$model->image_route,['class'=>'img-responsive']); ?>
  <?= \yii\widgets\DetailView::widget([
      'model' => $model,
      'attributes' => [
          'content:ntext:'
        /*  'label'=>'',
          'attribute'=>'content',
          'format'=>'ntext'*/
      ]
  ]
      ); ?>
    <div class="meta">
        <p><?=Yii::t('app','Publish date: {0,date,full}-{0,time}',['0'=>strtotime($model->publish_date)])?></p>
        <p><?=Yii::t('app','Update date: {0,date,full}-{0,time}',['0'=>strtotime($model->update_date)])?></p>
    </div>

</div>