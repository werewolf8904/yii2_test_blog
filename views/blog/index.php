<?php
/**
 * Created by PhpStorm.
 * User: Yurik
 * Date: 29.01.2017
 * Time: 11:27
 */

use yii\helpers\Html;
use app\models\Post;
use yii\widgets\LinkPager;
?>
<div class="col-sm-8 post-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    foreach ($posts as $post) {
        echo $this->render('_shortView', [
            'model' => $post
        ]);
    }
    ?>
    <?= LinkPager::widget(['pagination' => $pagination]) ?>
</div>
