<?php
/**
 * Created by PhpStorm.
 * User: Yurik
 * Date: 29.01.2017
 * Time: 11:42
 */
use app\models\Post;
use yii\helpers\Html;
use \yii\helpers\StringHelper;
?>

    <h3><?= Html::a($model->title, ['view', 'slug' => $model->slug]) ?></h3>

    <div class="content" widht="700">
        <?= StringHelper::truncate($model->content,300,'...');  ?>
        <?= Html::img(Yii::$app->params['thumbs_root'].$model->image_route,['class'=>'img-responsive','width'=>100,'height'=>100]); ?>
        
    </div>