<?php

use yii\db\Migration;
use app\models\Post;
use yii\db\Schema;

/**
 * Ініціалізація бази
 */
class m170129_134654_install extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%post}}', [
            'id' => Schema::TYPE_UPK,
            'image_route'=>Schema::TYPE_STRING . ' NOT NULL',
            'publish_status' => "enum('" . Post::STATUS_DRAFT . "','" . Post::STATUS_PUBLISH . "') NOT NULL DEFAULT '" . Post::STATUS_DRAFT . "'",
            'slug' => Schema::TYPE_STRING. ' NOT NULL',
            'publish_date' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'update_date' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ], $tableOptions);
        $this->createIndex('slug', '{{%post}}', 'slug',true);

        $this->createTable('{{%postlang}}', [
            'id' => Schema::TYPE_UPK,
            'post_id'=>$this->integer(10)->unsigned()->notNull(),
            'language'=>$this->char(6)->notNull(),
            'title'=>$this->char(255)->notNull(),
            'content'=>$this->text()->notNull()
        ], $tableOptions);

        $this->createIndex('post_id','{{%postlang}}','post_id');
        $this->createIndex('language','{{%postlang}}','language');

        $this->addForeignKey('postlang','{{%postlang}}','post_id','{{%post}}','id','CASCADE','CASCADE');
    }


    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('postlang','{{%postlang}}');
        $this->dropTable('{{%post}}');
        $this->dropTable('{{%postlang}}');

    }
}
