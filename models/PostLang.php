<?php
/**
 * Created by PhpStorm.
 * User: Yurik
 * Date: 31.01.2017
 * Time: 20:28
 */

namespace app\models;
use yii\db\ActiveRecord;

/**
 * Таблиця мультимовності
 * Class PostLang
 * @package app\models
 */
class PostLang extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%postLang}}';
    }

    /**
     * Звязок з основною таблицею постів
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }
}