<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\post;
use omgdef\multilingual\MultilingualBehavior;

/**
 * postsearch represents the model behind the search form about `\app\models\post`.
 */
class postsearch extends post
{
    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => \Yii::$app->params['translatedLanguages'],
                'requireTranslations' => true,
                'langClassName' => PostLang::className(), // or namespace/for/a/class/PostLang
                'defaultLanguage' => \Yii::$app->params['defaultLanguage'],
                'langForeignKey' => 'post_id',
                'tableName' => "{{%postLang}}",
                'attributes' => [
                    'title', 'content',
                ]
            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['image_route', 'publish_status', 'slug', 'publish_date', 'update_date','title','content'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = post::find()->joinWith('postLangs')->andFilterWhere(['language'=>Yii::$app->language]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'publish_date' => $this->publish_date,
            'update_date' => $this->update_date,
        ]);

        $query->andFilterWhere(['like', 'image_route', $this->image_route])
            ->andFilterWhere(['like', 'publish_status', $this->publish_status])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}
