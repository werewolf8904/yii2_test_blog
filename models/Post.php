<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%post}}".
 *
 * @property string $id
 * @property string $title
 * @property string $content
 * @property string $image_route
 * @property string $publish_status
 * @property string $publish_date
 * @property string $update_date
 */
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\imagine\Image;
use omgdef\multilingual\MultilingualBehavior;
use omgdef\multilingual\MultilingualQuery;

class Post extends ActiveRecord
{

    /**
     * Статус поста: опублікований.
     */
    const STATUS_PUBLISH = 'publish';
    /**
     * Статус поста: неопоблікований.
     */
    const STATUS_DRAFT = 'draft';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%post}}';
    }

    const SCENARIO_CREATE = 'create';
    /**
     * Змінна для загрузки фотографій
     */
    public $image;

    /**
     * Підключення behaviors для генерації slug і мультимовності бази
     * @return array
     */
    public function behaviors()
    {
        return [
            'slug' => [
                // 'class' => 'yii\behaviors\SluggableBehavior',
                //SluggableBehaviorAlwaysNewSlug
                'class' => 'app\components\SluggableBehaviorAlwaysNewSlug',
                'slugAttribute' => 'slug',
                'attribute' => 'title',
                // optional params
                'ensureUnique' => true,
                'immutable' => false,

            ],
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => \Yii::$app->params['translatedLanguages'],
                //'languageField' => 'language',
                //'localizedPrefix' => '',
                'requireTranslations' => true,
                //'dynamicLangClass' => true',
                'langClassName' => PostLang::className(), // or namespace/for/a/class/PostLang
                'defaultLanguage' => \Yii::$app->params['defaultLanguage'],
                'langForeignKey' => 'post_id',
                'tableName' => "{{%postLang}}",
                'attributes' => [
                    'title', 'content',
                ]
            ]
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'image_route', 'publish_status'], 'required'],
            ['image', 'required', 'on' => Post::SCENARIO_CREATE],
            [['content', 'publish_status'], 'string'],
            [['publish_date', 'update_date'], 'safe'],
            [['title'], 'string', 'max' => 255, 'min' => 5],
            ['image', 'image', 'extensions' => 'png, jpg', 'maxHeight' => 4000, 'maxWidth' => 3000, 'maxSize' => 2097152]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'image_route' => Yii::t('app', 'Image'),
            'publish_status' => Yii::t('app', 'Publish status'),
            'slug' => Yii::t('app', 'Slug'),
            'publish_date' => Yii::t('app', 'Publish date'),
            'update_date' => Yii::t('app', 'Update date'),
            'content'=> Yii::t('app', 'Content'),
            'title'=> Yii::t('app', 'Title'),
        ];
    }

    /**Отримання опублікованих постів
     * @param null $pagination
     * @return array|ActiveDataProvider|\yii\db\ActiveRecord[]
     */
    public function getPublishedPosts($pagination = null)
    {
        if ($pagination === null) {
            return new ActiveDataProvider([
                'query' => Post::find()
                    ->where(['publish_status' => 'publish'])
                    ->orderBy(['publish_date' => SORT_DESC])
            ]);
        } else {

            return Post::find()
                ->where(['publish_status' => 'publish'])
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->orderBy(['publish_date' => SORT_DESC])
                ->all();

        }


    }

    /**
     * Додатково завантаження і оновлення фото
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     *
     * @inheritdoc
     */
    public function Save($runValidation = true, $attributeNames = null)
    {

        $this->image = UploadedFile::getInstance($this, 'image');

        if ($this->image !== null)
        {
            //ящо при оновленні отримано нове фото-видалити старі
            if ((!$this->isNewRecord) && (file_exists(Yii::getAlias('@webroot'.Yii::$app->params['image_root']) . $this->image_route))) {
                unlink(Yii::getAlias(Yii::getAlias('@webroot'.Yii::$app->params['image_root']) . $this->image_route));
            }
            if ((!$this->isNewRecord) && (file_exists(Yii::getAlias('@webroot'.Yii::$app->params['thumbs_root']) . $this->image_route))) {
                unlink(Yii::getAlias(Yii::getAlias('@webroot'.Yii::$app->params['thumbs_root']) . $this->image_route));
            }
            //створення імя фото для уникнення співпадінь
            $this->image_route = Yii::$app->security->generateRandomString(3) . '_'.$this->image->baseName.'.' . $this->image->extension;
            //збереження фото і генерація його обрізку
            if ($this->image->saveAs(Yii::getAlias('@webroot'.Yii::$app->params['image_root']) . $this->image_route))
            {
                Image::thumbnail(Yii::getAlias('@webroot'.Yii::$app->params['image_root']) . $this->image_route, 100, 100)
                    ->save(Yii::getAlias('@webroot'.Yii::$app->params['thumbs_root'] . $this->image_route), ['quality' => 80]);
                $this->image = $this->image_route; // без этого ошибка
                return parent::save($runValidation, $attributeNames);
            } else return false;
        }
        else
            return parent::save($runValidation, $attributeNames);
    }

    /**
     * Додатково видаляються фото перед видаленням запису з бази
     *
     * @return bool|false|int
     * @throws \Exception
     *
     */
    public function Delete()
    {
        if ((file_exists(Yii::getAlias('@webroot'.Yii::$app->params['image_root']) . $this->image_route))
            && (file_exists(Yii::getAlias('@webroot'.Yii::$app->params['thumbs_root']) . $this->image_route)))
        {
            unlink(Yii::getAlias(Yii::getAlias('@webroot'.Yii::$app->params['image_root']) . $this->image_route));
            unlink(Yii::getAlias(Yii::getAlias('@webroot'.Yii::$app->params['thumbs_root']) . $this->image_route));
            return parent::delete();
        } else
            return false;

    }

    /**
     * для MultilanguageBehavior
     */
    public static function find()
    {
        return new MultilingualQuery(get_called_class());

    }

    /**
     * Звязок з таблицею де зберігаються мультимовні стовбці
     * @return \yii\db\ActiveQuery
     */
    public function getPostLangs()
    {
        return $this->hasMany(PostLang::className(), ['post_id' => 'id']);
    }
}